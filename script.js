const toggleButton = document.querySelector(".navbar-toggle-btn");
const navBar = document.querySelector(".navbar-nav");
const hamIcon = document.getElementById("icon-hamburger");

function toggleNavBar() {
  toggleButton.classList.toggle("active");
  navBar.classList.toggle("active");

  if (hamIcon.src.endsWith("burger.svg")) {
    hamIcon.src = "images/icon-close.svg";
  } else {
    hamIcon.src = "images/icon-hamburger.svg";
  }
}

toggleButton.addEventListener("click", toggleNavBar);
